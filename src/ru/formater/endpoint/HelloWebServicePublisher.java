package ru.formater.endpoint;

import ru.formater.svc.HelloWebServiceImpl;

import javax.xml.ws.Endpoint;

public class HelloWebServicePublisher {
    public static void main(String[] args) {
        Endpoint.publish("http://localhost:1985/wss/hello", new HelloWebServiceImpl());
    }
}
