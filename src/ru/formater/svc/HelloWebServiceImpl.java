package ru.formater.svc;

import javax.jws.WebService;

@WebService(endpointInterface = "ru.formater.svc.HelloWebService")
public class HelloWebServiceImpl implements HelloWebService {
    @Override
    public String getHelloString(String name) {
        return "Hello, " + name + "!";
    }
}
